<?php

/**
 * Dba cache backend
 *
 * Serialization with php serialize/unserialize.
 */
class dbaCache implements DrupalCacheInterface {
  protected $bin;

  protected $dba_handler;

  function __construct($bin) {
    $this->bin = $bin;
    $this->dba_handler = dba_open($this->getFileName(), 'c');
  }

  function getFileName() {
    // @todo choose unpublic folder.
    $folder = variable_get('file_private_path', conf_path() . '/files');
    $filename = $folder . '/' . $this->bin .'.dba';

    return $filename;
  }

  /**
   * Check if a cache bin is empty.
   *
   * A cache bin is considered empty if it does not contain any valid data for
   * any cache ID.
   *
   * @return
   *   TRUE if the cache bin specified is empty.
   */
  function isEmpty() {
    $key = dba_firstkey($this->dba_handler);
    return is_null($key);
  }

  /**
   * Expire data from the cache. If called without arguments, expirable
   * entries will be cleared from the cache_page and cache_block bins.
   *
   * @param $cid
   *   If set, the cache ID to delete. Otherwise, all cache entries that can
   *   expire are deleted.
   * @param $wildcard
   *   If set to TRUE, the $cid is treated as a substring
   *   to match rather than a complete ID. The match is a right hand
   *   match. If '*' is given as $cid, the bin $bin will be emptied.
   */
  function clear($cid = NULL, $wildcard = FALSE) {
    // @todo Implement clear() method.

    // Sure this isn't performant :)
    dba_close($this->dba_handler);
    $filename = $this->getFileName();
    // http://php.net/manual/en/function.dba-open.php
    $this->dba_handler = dba_open($filename, 'c');
  }

  /**
   * Store data in the persistent cache.
   *
   * @param $cid
   *   The cache ID of the data to store.
   * @param $data
   *   The data to store in the cache. Complex data types will be automatically
   *   serialized before insertion.
   *   Strings will be stored as plain text and not serialized.
   * @param $expire
   *   One of the following values:
   *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
   *     explicitly told to using cache_clear_all() with a cache ID.
   *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
   *     general cache wipe.
   *   - A Unix timestamp: Indicates that the item should be kept at least until
   *     the given time, after which it behaves like CACHE_TEMPORARY.
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $fields = array();
    $fields['created'] = REQUEST_TIME;
    $fields['expire'] = $expire;
    $fields['data'] = $data;

    $stream = serialize($fields);
    $ret = dba_replace($cid, $stream, $this->dba_handler);
  }

  /**
   * Return data from the persistent cache when given an array of cache IDs.
   *
   * @param $cids
   *   An array of cache IDs for the data to retrieve. This is passed by
   *   reference, and will have the IDs successfully returned from cache
   *   removed.
   *
   * @return
   *   An array of the items successfully returned from cache indexed by cid.
   */
  function getMultiple(&$cids) {
    // @todo check memcached how they did it
    // http://drupalcontrib.org/api/drupal/contributions%21memcache%21memcache.inc/7

    $result = array();
    foreach ($cids as $cid) {
      $stream = dba_fetch($cid, $this->dba_handler);
      if ($stream) {
        $fields = unserialize($stream);
        if ($fields['data'] !== FALSE) {
          unset($cids[$cid]);
          $result[$cid] = (object) $fields;
        }
      }
    }
    return $result;
    // @todo there could be more performant ideas.
  }

  /**
   * Return data from the persistent cache. Data may be stored as either plain
   * text or as serialized data. cache_get will automatically return
   * unserialized objects and arrays.
   *
   * @param $cid
   *   The cache ID of the data to retrieve.
   *
   * @return
   *   The cache or FALSE on failure.
   */
  function get($cid) {
    $stream = dba_fetch($cid, $this->dba_handler);
    if ($stream) {
      $fields = unserialize($stream);
      if ($fields['data'] !== FALSE) {
        return (object) $fields;
      }
    }
    return FALSE;
  }
}
